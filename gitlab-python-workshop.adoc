= Gitlab with Python Workshop
Arkadiusz Tułodziecki <atulodzi@gmail.com>
1.0, Jun 30, 2021: AsciiDoc article template
:toc:
:source-highlighter: rouge
:encoding: utf-8
:icons: font
:url-quickref: https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/

ifdef::backend-html5[]
PDF version of this article:
link:gitlab-python-workshop.pdf/[gitlab-python-workshop.pdf]
endif::[]

== Python Backend CI example

We will ne creating basic API project using Python language and Flask Framework.
Complete code can be found here: https://gitlab.com/mobica-workshops/examples/python/flask/simple-development and you can just check your code against it if any problem will occur.

*Because Gitlab Shared Pipelines were used for the Bitcoin mining to use them you need to add to your account credit cart or register own runner* if you plan to use own username space or own Gitlab group for this training

In this case if you can't or do not want to use own credit cart to activate public shared runners for your project please use this manual: https://mobica-workshops.gitlab.io/documentation/gitlab-runner-k3d-guide/ prior to the next steps.

=== Core project creation

You need latest pythonfootnote:[Python download: https://www.python.org/downloads/] with pipfootnote:[Pip installation: https://pip.pypa.io/en/stable/installing/]
and virtualenvwrapperfootnote:[Virtualenv wrapper install: https://virtualenvwrapper.readthedocs.io/en/latest/].
You also need a Gitlab account at the gitlab.com page.

First, you will need to create a git repository in the gitlab.com which will be used for our project, lets name it `ex-flask-dev`. You will need to clone this empty repository in the location of your choose.

==== Requirements

We will start with basic requirements files. Please create `requirements` folder and put in it `common.txt` file with this content:

[source,text]
----
include::materials/requirements/common.txt[]
----

We will just install with it click, Flask framework and Flask-Cors module.

Lets create our virtual env and use this file:

[source,bash]
----
mkvirtualenv ex-flask-dev
pip install -r requirements/common.txt
----

with this we will have required libraries to just start work.

==== Central controller and first view

Now we need to prepare out base app core structure. Please create `app` folder and inside of this folder `main` sub folder.
In the `app/main` folder we will create a `views.py` file with this content:

[source,python]
----
include::materials/app/main/views.py[]
----

We will treat `main` folder as a package for the main blueprints and we also needs a `__init__.py` file like this:

[source,python]
----
include::materials/app/main/__init__.py[]
----

Our `app` folder also will ne a package and will have `create_app` function which will be using our views like created upper and other potential files like models and forms we can add in the future.
Will be also our central controller. Please create `__init__.py` file like this in this folder:

[source,python]
----
include::materials/app/__init__.py[]
----

==== Core application files

We will now need a core Flask file and core configuration files which will be configuring Flask and using our app.

Let's start with creating `config.py` file in the root folder like this:

[source,python]
----
include::materials/config.py[]
----

Next we need to create our `api.py` file in the root folder like this:

[source,python]
----
include::materials/api.py[]
----

which will be a core Flask file we will give to `FLASK_APP` environment variable

==== First check

We need now only a `FLASK_APP` and `FLASK_ENV` set to check if our app works

[source,bash]
----
export FLASK_APP=api.py
export FLASK_ENV=development
----

we can run our application now with the `flask run` command.

You can check your app by just going to: http://127.0.0.1:5000/

After this check we will just add a `.gitignore` file in the root folder looking like this:

[source,text]
----
include::materials/.gitignore[]
----

and lets commit and push to the Gitlab files we already created.

=== Tests

Normally we should start with them but in this case we just add them now. Please remember that next views and code should be done with the TDD methodology.

Let's start with progressing out development environment.

==== Requirements

We need to create `development.txt` file in the `requirements` folder looking like this:

[source,text]
----
include::materials/requirements/development.txt[]
----

also to made a standard development even more easy we will create `requirements.txt` in the root folder looking like this:

[source,text]
----
include::materials/requirements.txt[]
----

Now we just need to install `coverage`, `flake8` and `isort` which we will be using for the testing with the command below:

[source,bash]
----
pip install -r requirements.txt
----

==== First test file

Our core `api.py` file was already prepared to use `coverage` and we now only needs to prepare `tests` folder which we will need to treat as the package.
For this we need a empty `__init__.py` file. After creating this file we just need to add our first test file `test_basics.py` which will look like this:

[source,python]
----
include::materials/tests/test_basics.py[]
----

Now it is possible to test our app with command `flask test --coverage`.

==== Flake8

For the next type of the test, lets add configuration for the `flake8` which is checking if our code meets PEP standards.
We need to create `.flake8` file in the root folder looking like this:

[source,text]
----
include::materials/.flake8[]
----

After adding this file we can just check if out code is ok with command: `flake8 app`. If result is empty all is OK.

==== Isort

Last test we planning to use is `isort` what is even more strict then PEP and checking if our imports are as readable as possible.
We can check this by using command: `isort --diff --check-only app`. If result is empty all is OK.

Now lets commit our changes and push to the Gitlab.

=== Docker

We like to have our application in the container what will help us to install it in the modern environment like for example in the Kubernetes cluster.
For this we need to have dockerfootnote:[Docker: https://docs.docker.com/engine/install/] and docker-composefootnote:[Docker Compose: https://docs.docker.com/compose/install/].

==== Requirements

We need to create `docker.txt` file in the `requirements` folder looking like this:

[source,text]
----
include::materials/requirements/docker.txt[]
----

==== Docker files

We will go with a little more advanced configuration as we will plan to have in the future a development, test and production environments which will work differently.

Lets start by creating `boot.sh` file looking like this:

[source,bash]
----
include::materials/boot.sh[]
----

next will ne our `cmd.sh` file looking like this:

[source,bash]
----
include::materials/cmd.sh[]
----

and finally our `Dockerfile` which will be using upper files looking like this:

[source,text]
----
include::materials/Dockerfile[]
----

Now we can build oud Docker image, but lets do this with Docker Compose.

==== Docker Compose

Lets create our `docker-compose.yml` file which we will use as a starting point looking like this:

[source,yaml]
----
include::materials/docker-compose.yml[]
----

Now we can build our image with the `docker-compose build` command and start it with the `docker-compose up` command.

Now lets commit our changes and push to the Gitlab.

=== CI/CD

Now we are missing CI/CD in our project. In the Gitlab we need to create `.gitlab-ci.yml` file which will instruct Gitlab how to achieve this.
We can even have Auto DevOps with gitlab created but let's focus on the simpler pipeline. Please create file like this:

[source,yaml]
----
include::materials/.gitlab-ci.yml[]
----

Now commit and push and check pipeline.

image:https://i.creativecommons.org/l/by/4.0/88x31.png[Creative Commons Licence,88,31] This work is licensed under a http://creativecommons.org/licenses/by/4.0/[Creative Commons Attribution 4.0 International License]