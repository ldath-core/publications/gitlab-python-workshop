#!/bin/bash
set -e
source venv/bin/activate

if [[ "$FLASK_ENV" = 'development' ]]; then
    echo "Running Development Server"
    exec flask run --host=0.0.0.0 --port=5000
elif [[ "$FLASK_ENV" = 'test' ]]; then
    echo "Running Tests"
    exec flask test
else
    echo "Running Production Server"
    exec gunicorn -b :5000 -k gevent --access-logfile - --error-logfile - api:app
fi
